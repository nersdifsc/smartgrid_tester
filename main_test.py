# -*- coding: utf-8 -*-

import paho.mqtt.client as mqtt
import time
import datetime
import random


# Função para obter tempo em epoch Unix (milisegundos desde 1 Jan. 1970)
def unix_time_millis(dt):
    epoch = datetime.datetime.utcfromtimestamp(0)
    return (dt - epoch).total_seconds() * 1000.0


# Função executada ao iniciar conexão mqtt
def on_connect(client, userdata, flags, rc):
    print("Cliente MQTT conectado com status {}".format(rc))


# Função executada ao receber mensagem
def on_message(client, userdata, message):
    print("Recebida mensagem. Payload: {}".format(message.payload))
    
    
# Simular uma entrada no banco de dados, com timestamp opcional (se omitida, marcar tempo atual)
def sim_ponto((v, i), dt=datetime.datetime.now()):
    
    # Simular tensão aleatoriamente; se estiver muito acima ou abaixo de 220V, pesamos para 220V
    if v < 210:
        v += random.uniform(-2, 5)
    elif v > 230:
        v += random.uniform(-5, 2)
    else:
        v += random.uniform(-3, 3)
    
    # Simular corrente aleatoriamente; se estiver muito acima ou abaixo de 3A, pesamos para 3A
    # Às vezes jogamos a corrente a 0 para simular o desligamento de um aparelho
    if i == 0:
        if random.randint(0, 20) == 0:
            i = random.uniform(2, 4)  # Simular religamento (chance de 1 em 20)
    else:
        if random.randint(0, 35) == 0:
            i = 0  # Simular desligamento (chance de 1 em 35)
        elif i > 4:
            i += random.uniform(-0.4, 0.2)
        elif i < 2:
            i += random.uniform(-0.2, 0.4)
        else:
            i += random.uniform(-0.2, 0.2)

    timestamp = unix_time_millis(dt)

    topic_v = '/smartgrid/update/computador-1/tensao/{:d}'.format(int(timestamp))
    topic_i = '/smartgrid/update/computador-1/corrente/{:d}'.format(int(timestamp))
    mqttc.publish(topic_v, v)
    mqttc.publish(topic_i, i)

    topic_v = '/smartgrid/update/ventilador-1/tensao/{:d}'.format(int(timestamp))
    topic_i = '/smartgrid/update/ventilador-1/corrente/{:d}'.format(int(timestamp))
    mqttc.publish(topic_v, pow(v, 1.1))
    mqttc.publish(topic_i, pow(i, 0.7))

    return v, i

    
# Função para simulação de dados passados
# Simula 600 pontos de leitura com espaçamento de 5 minutos
def sim_dados((v, i)):
    for j in range(0, 15000):
        v, i = sim_ponto((v, i), datetime.datetime.now() - datetime.timedelta(minutes=5*j))
    return v, i


# ########################################################################################  #
# ####################### INÍCIO DE PROGRAMA #############################################  #
# ########################################################################################  #


# Inicializar a instância de cliente MQTT
mqttc = mqtt.Client()

# Setar callbacks
mqttc.on_connect = on_connect
mqttc.on_message = on_message

# Conectar e iniciar thread que escuta MQTT (não bloqueia)
mqttc.connect('localhost')
mqttc.loop_start()

# Criar variáveis para simulação de tensão e corrente
sim_v = 220
sim_i = 2

# Simular histórico
#sim_v, sim_i = sim_dados((sim_v, sim_i))

while True:
    # time.sleep(5 * 60)  # Esperar 5 minutos entre transmissões
    time.sleep(1)  # Esperar 5 minutos entre transmissões

    sim_v, sim_i = sim_ponto((sim_v, sim_i), datetime.datetime.now())


'''
    except:
        # Desconectar thread MQTT
        mqttc.loop_stop()
        mqttc.disconnect()
'''
